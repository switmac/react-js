import {RETRIEVE_GATES} from '../actions/action-gates';

export default function (state = [], action) {
  switch (action.type) {
    case RETRIEVE_GATES:
      return action.payload.data;
  }

  return state;
}