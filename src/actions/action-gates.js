import axios from 'axios';

export const CREATE_GATE = 'CREATE_GATE';
export const RETRIEVE_GATES = 'RETRIEVE_GATES';
export const UPDATE_GATE = 'UPDATE_GATE';
export const DELETE_GATE = 'DELETE_GATE';

const gates_baseurl = '/gates'

export function createGate(props) {
	const request = axios.post(gates_baseurl, props);

	return {
		type: CREATE_GATE,
		payload: request
	}
}

export function retrieveGates() {
	const request = axios.get(`${gates_baseurl}/1`);

	return {
		type: RETRIEVE_GATES,
		payload: request
	}
}

export function updateGate(props) {
	const request = axios.put(`${gates_baseurl}/${props.id}`, props);

	return {
		type: UPDATE_GATE,
		payload: request
	}
}

export function deleteGate(props) {
	const request = axios.delete(`${gates_baseurl}/${props.id}`, props);

	return {
		type: DELETE_GATE,
		payload: request
	}
}