import axios from 'axios';

export const RETRIEVE_PATHS = 'RETRIEVE_PATHS';
export const RETRIEVE_PATH = 'RETRIEVE_PATH';

export function retrievePaths(host) {
  const request = axios.get(`/pages/${host}`);
  return {
    type: RETRIEVE_PATHS,
    payload: request
  }
}

export function retrievePath(path) {
  const request = axios.get(`page/${path}`);
  return {
    type: RETRIEVE_PATH,
    payload: request
  }
}