import filter from 'lodash/filter';
import omit from 'lodash/omit';
import uuid from 'node-uuid';

import {SHOW_NOTIFICATION, HIDE_NOTIFICATION} from '../actions/action-notification';

export function Notifications(state = [], action) {
  switch (action.type) {
    case SHOW_NOTIFICATION :
      return [
        ...state,
        {...omit(action, 'type'), uid: uuid.v4()}
      ];
    case HIDE_NOTIFICATION :
      return filter(state, (notification) => {
        return notification.uid !== action.uid;
      });
  }

  return state;
}




