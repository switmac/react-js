import React, {Component, PropTypes} from 'react';
import {reduxForm} from 'redux-form';
import {Modal, Button} from 'react-bootstrap';

import {retrieveGates} from '../actions/action-gates';
import {saveCampaign} from '../actions/action-campaigns';
import Rule from '../components/rule';

require('../styles/style.css');

export const fields = [
	'path',
	'rules[].gate',
	'rules[].state',
	'rules[].page'
];

class Rules extends Component {

	constructor(props) {
		super(props);
		this.state = {visible: false};

		this.onOpen = this.onOpen.bind(this);
		this.onClose = this.onClose.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
		this.renderFilters = this.renderFilters.bind(this);
	}

	componentWillMount() {
		this.props.retrieveGates();
	}

	onClose() {
		this.setState({visible: false});
		this.resetForm();
	}

	onOpen() {
		this.setState({visible: true});
	}

	onSubmit(props) {
		this.props.saveCampaign(props);
	}

	setStatus(status) {
		if (this.state.status === status) {
			this.setState({status: ''});
		} else {
			this.setState({status});
		}
	}

	resetForm() {
		this.props.resetForm("RulesForm");
	}

	renderFilters(filters) {
		return this.props.gates.map(function (filter, index) {
			return (
			  <tr key={index}>
				  <td>
					  <input
						type="checkbox"
						name="filters"
						value={filter.name}
						onChange={event =>{
                if(event.target.checked){
                  filters.addField(event.target.value);
                } else {
                  filters.map(function(object, index) {
                    if(object.value == event.target.value){
                       filters.removeField(index);
                    }
                  });
                }
              }}
					  />
				  </td>
				  <td>{filter.name}</td>
				  <td>{filter.property}</td>
				  <td>{filter.status}</td>
				  <td>{filter.values}</td>
			  </tr>
			);
		});
	}

	render() {
		const {
		  path,
		  host,
		  fields :{rules, page},
		  handleSubmit
		} = this.props;

		return (
		  <span className="left-margin">
        <button className="btn btn-secondary" onClick={this.onOpen}>Manage Rules</button>

        <Modal show={this.state.visible} onHide={this.onClose}>
          <Modal.Header closeButton>
            <h6 className="text-muted">RULES</h6>
            <Modal.Title>{host}/{path}</Modal.Title>
          </Modal.Header>
          <form>
          <Modal.Body>
              <button
                className="btn btn-primary"
                onClick={(event) => {
                event.preventDefault();
                console.log(rules);
                  rules.addField();
                }}
              >
                <i className="fa fa-plus" aria-hidden="true"></i>
                <span className=" left-margin">Add Rule</span>
              </button>
	          {!rules.length && <div className="rule-entry">No Rules</div>}
	          {
		          rules.map((rule, index) => {
			          return (
			            <Rule
				          removeCb={rules.removeField}
				          gates={this.props.gates}
				          rule={rule}
				          index={index}/>
			          );
		          })
	          }
          </Modal.Body>
          <Modal.Footer>
            <div>
                <button className="btn btn-primary" type="submit">Submit</button>
                <Button className="btn btn-secondary left-margin" onClick={this.onClose}>Cancel</Button>
              </div>
          </Modal.Footer>
          </form>
        </Modal>
      </span>
		);
	}
}

function mapStateToProps({gates}) {
	return {gates};
}


Rules.propTypes = {
	fields: PropTypes.object.isRequired,
	handleSubmit: PropTypes.func.isRequired,
	resetForm: PropTypes.func.isRequired
}

export default reduxForm(
  {
	  form: 'RulesForm',
	  fields,
	  null
  },
  mapStateToProps,
  {
	  saveCampaign,
	  retrieveGates
  })(Rules);