import React, {Component} from 'react';
require('../styles/style.css');

class Rule extends Component {
  constructor(props) {
    super(props);
  }

  renderGates() {
    return this.props.gates.map((gate, index) => {
      return (<option key={index} value={gate.name}>{gate.name}</option>);
    });
  }

  render() {
    const {
      rule,
      index,
      removeCb
    } = this.props;

    return (
      <div key={index} className="form-group form-inline rule-entry">
        <label>Rule No. {index+1}</label>
        <select className="left-margin gate-select form-control" {...rule.gate}>
          <option></option>
          {this.renderGates()}
        </select>
        <input
          type="checkbox"
          className="switch switch-success left-margin"
          data-label-on="ALLOW"
          data-label-off="DENY"
          {...rule.state}
        />
        <input
          type="url"
          className="left-margin form-input form-control"
          placeholder="alternative page"
          {...rule.page}
        />
        <button
          className="btn btn-danger btn-icon left-margin"
          onClick={(event) => {
            event.preventDefault();
            removeCb(index);
          }}
        ><i className="fa fa-times" aria-hidden="true"></i>
        </button>
      </div>
    );
  }
}

export default Rule;



