import React, {Component} from 'react';
import {Link} from 'react-router';
import {connect} from 'react-redux';
import {Button, ButtonGroup} from 'react-bootstrap';

import {saveDomainAction} from '../actions/action-campaigns';

class DomainEntry extends Component {

  constructor(props) {
    super(props);

    this.state = {action: this.props.domain.action};
  }

  isSelected(value) {
    return `${(value === this.state.action) ? 'active' : 'default'}`;
  }

  statusHandler(action) {
    this.setState({action});
    this.props.saveDomainAction({action});
  }

  render() {
    const {host, ratio, trust_flow, citation_flow} = this.props.domain;
    const {key} = this.props;

    return (
      <tr key={`${key}`}>
        <td key={`${host}-host`}><Link to={`/campaigns/${host}`}>{host}</Link></td>
        <td key={`${host}-trust-flow`}>{trust_flow}</td>
        <td key={`${host}-citation-flow`}>{citation_flow}</td>
        <td key={`${host}-ratio`}>{ratio}</td>
        <td key={`${host}-action`}>
          <ButtonGroup>
            <Button
              onClick={() => this.statusHandler("Live")}
              className={this.isSelected('Live')+ " btn-primary radio-button-left"}>
              Live
            </Button>
            <Button
              onClick={() => this.statusHandler("Paused")}
              className={this.isSelected('Paused')+ " btn-primary radio-button-left"}>
              Paused
            </Button>
            <Button
              onClick={() => this.statusHandler("Deleted")}
              className={this.isSelected('Deleted')+ " btn-primary radio-button-left"}>
              Delete
            </Button>
          </ButtonGroup>
        </td>
      </tr>
    );
  }
}

export default connect(null, {saveDomainAction})(DomainEntry);