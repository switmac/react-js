import axios from 'axios';

export const RETRIEVE_CUSTOMERS = 'RETRIEVE_CUSTOMERS';

export function retrieveCustomers() {
  const request = axios.get('/customers');

  return {
    type: RETRIEVE_CUSTOMERS,
    payload: request
  }
}