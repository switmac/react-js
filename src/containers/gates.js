import React, {Component} from 'react';
import {connect} from 'react-redux';
import _ from 'lodash';

import {retrieveGates, deleteGate} from '../actions/action-gates';
import Table from '../components/table';
import GatesForm from '../containers/gates-form';
import Confirmation from '../components/confirmation';

class Gates extends Component {

	constructor(props) {
		super(props);

		this.state = {active: 0, gate: ''};
		this.selectGate = this.selectGate.bind(this);
		this.isSelected = this.isSelected.bind(this);
	}

	componentWillMount() {
		this.props.retrieveGates()
		  .then(() => {
			  this.setGate();
		  });
	}

	isSelected(value) {
		return `${(value === this.state.active) ? 'active' : ''}`;
	}

	setOptions() {
		var properties = ['City', 'Subdivision', 'Country', 'Continent',
			'IPAddress', 'ISP', 'Organization', 'Carrier', 'Platform', 'UserAgent', 'IsSatelliteProvider',
			'IsProxy', 'Mobile', 'Host', 'Referrer'];

		return properties.map((property) => {
			return (
			  <option key={property} value={property}>{property}</option>
			);
		});
	}

	selectGate(gate, index) {
		this.setState({gate: gate});
		this.setState({active: index});
	}

	setGates() {
		return this.props.gates.map((gate, index) => {
			return (
			  <a
				className={`${this.isSelected(index)} list-group-item`}
				key={index}
				onClick={() =>
            this.selectGate(gate, index)
          }
				href="#"
			  >
				  <strong>{gate.name}</strong>
			  </a>
			);
		});
	}

	setGate() {
		this.setState({gate: this.props.gates[this.state.active]});
	}

	render() {
		let heading = <div className="view-title">
			<h4>Gates</h4>
		</div>;


		if (!this.props.gates || !this.state.gate) {
			return (
			  <div id="gates">
				  {heading}
				  <Table
					headers={['Gate Name']}
					body={<tr><td className="colSpan">No Gates Found</td></tr>}
				  />
			  </div>
			);
		}

		return (
		  <div id="gates">
			  {heading}
			  <div className="row">
				  <div className="col-sm-4 gates-entry">
					  <GatesForm
						form="add"
						formKey="add"
						method="ADD"
						addGates={(data) =>{
							this.props.gates.unshift(data);
							this.setState({active : 0});
							this.setGate();
					  }}/>
					  <ul className="nav nav-sidebar nav-pills nav-stacked list-group gate-list">
						  {this.setGates()}
					  </ul>
				  </div>

				  <div className="row">
					  <div className="col-sm-8">
						  <div>
							  <Confirmation
								buttonStyle="btn btn-icon btn-danger btn-remove left-margin pull-xs-right"
								icon={<i className="fa fa-trash" aria-hidden="true"></i>}
								label={<span className="left-margin">Remove</span>}
								message="Are you sure you want to remove this gate?"
								callback={() => {
									console.log(this.props.gates[this.state.active]);
									this.props.deleteGate(this.props.gates[this.state.active])
									.then((value) => {
										console.log(value);
										_.remove(this.props.gates, (e)=> {
											return (e.id == value.payload.data);
										});

										if(this.state.active != 0){
											this.setState({active : this.state.active-1 });
										} else {
											this.setState({active : 0 });
										}

										this.setGate();
									});
								}}/>
							  <GatesForm
								form={`edit-${this.state.active}`}
								formKey={`edit-${this.state.active}`}
								method="EDIT"
								data={this.props.gates[this.state.active]}
								updateGates={(data) =>	{
									this.props.gates[this.state.active] = data;
									this.setGate();
							     }}/>
						  </div>
						  <div className="clearfix"/>
						  <h4>{this.state.gate.name}</h4>
						  {this.state.gate.options.map((option, index) => {
							  return (
								<fieldset className="gate-option card" key={index}>
									<h6 className="card-header">{option.property}</h6>
									<div className="card-block">
										{option.values.map((value, index) => {
											var separator = ',';
											if (index == (option.values.length - 1)) {
												separator = '';
											}
											return (
											  <span
												className="gate-option-value card-text"
												key={index}>
												  {value}{separator}
											  </span>
											);
										})}
									</div>
								</fieldset>
							  );
						  })}
					  </div>
				  </div>
			  </div>
		  </div>
		);
	}
}

function mapStateToProps({gates}) {
	return {gates};
}

export default connect(mapStateToProps, {retrieveGates, deleteGate})(Gates);