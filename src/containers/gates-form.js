import React, {Component} from "react";
import {reduxForm, removeArrayValue} from "redux-form";
import {Button, Modal} from "react-bootstrap";

import {createGate, updateGate} from "../actions/action-gates";
import GateOption from "../components/gate-option";
import Notify from "../components/notification-inline";
import {validate, validateValueEntry, validateGate, validateOptions} from "../containers/gate-form-validator";

require('../styles/style.css');

export const fields = [
	'id',
	'name',
	'options[].property',
	'options[].values[]'
];

class GateForm extends Component {

	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			property: '',
			value: '',
			error: false,
			submitted: false,
			method: props.method,
			data: this.props.data
		};

		this.onOpen = this.onOpen.bind(this);
		this.onClose = this.onClose.bind(this);
		this.onSubmit = this.onSubmit.bind(this);
	}

	resetFormData() {
		const {
		  fields : {options},
		  resetForm,
		  destroyForm,
		  initializeForm
		} = this.props;

		resetForm();
		destroyForm();
		initializeForm();
	}

	onClose() {
		this.setState({
			submitted: false,
			value: '',
			property: '',
			error: false,
			visible: false
		});
		this.resetFormData();
	}

	onOpen() {
		this.setState({visible: true});

		if (this.state.method == 'EDIT') {
			this.setData();
		}
	}

	onEnterValue(options, property, value) {

		var validation = validateValueEntry(options, property, value);

		if (validation.empty) {
			this.setState({error: true});
			this.setState({message: 'Please select property and input value.'});
			return;
		} else if (validation.propertyExists && validation.valueExists) {
			this.setState({error: true});
			this.setState({message: 'Value and Property inputs already exists.'});
			return;
		} else if (validation.propertyExists && !validation.valueExists) {
			this.setState({error: false});
			validation.option.values.addField(value);
			this.setState({value: ''});
		} else {
			this.setState({error: false});
			options.addField({
				property: property,
				values: [value]
			});
			this.setState({value: ''});
		}
	}

	onSubmit(values) {
		var validation = validateOptions(values);

		if (validation.optionEmpty) {
			this.setState({error: true});
			this.setState({message: 'Options Required'});
			return;
		}

		if (validation.propertyEmpty) {
			this.setState({error: true});
			this.setState({message: 'Property Required'});
			return;
		}

		if (validation.valueEmpty) {
			this.setState({error: true});
			this.setState({message: 'Value Required'});
			return;
		}

		let callback = () => {
			this.setState({error: false});
			this.setState({submitted: true});
			setTimeout(this.onClose, 2000);
		};

		if (this.state.method == 'ADD') {
			this.props.createGate(values)
			  .then((value) => {
				  callback();
				  this.props.addGates(value.payload.data);
			  });

		} else {
			this.props.updateGate(values)
			  .then((value) => {
				  callback();
				  this.setState({data: value.payload.data});
				  this.props.updateGates(value.payload.data);
			  });
		}
	}

	setOptions() {
		var properties = ['City', 'Subdivision', 'Country', 'Continent',
			'IPAddress', 'ISP', 'Organization', 'Carrier', 'Platform', 'UserAgent', 'IsSatelliteProvider',
			'IsProxy', 'Mobile', 'Host', 'Referrer'];

		return properties.map(function (property) {
			return (
			  <option key={property} value={property}>{property}</option>
			);
			AddButton
		});
	}

	setData() {
		const {
		  id, name, options
		} = this.props.fields;

		id.onChange(this.state.data.id);
		name.onChange(this.state.data.name);
		this.state.data.options.map((property, index) => {
			var propertyObj = {property: property.property, values: property.values};
			options.addField(propertyObj);
		});
	}

	render() {
		const {
		  fields: {id, name, options},
		  handleSubmit
		} = this.props;

		let addForm =
		  <fieldset>
			  <div className="form-inline form-group">
				  <label>Value</label>
				  <select
					placeholder="Select Property"
					className="form-control left-margin"
					onChange={(event) => {
                      this.setState({property: event.target.value});
                    }}
					value={this.state.property}
				  >
					  <option key={0}></option>
					  {this.setOptions()}
				  </select>
				  <input
					type="text"
					className="form-control left-margin"
					placeholder="value"
					onChange={(event) => {
                      this.setState({value: event.target.value});
                    }}
					onEnter={(event) => {
                       this.setState({value: event.target.value});
                       event.preventDefault();
                       this.onEnterValue(options, this.state.property,this.state.value);
                    }}
					value={this.state.value}
				  />
				  <button
					className="btn btn-primary btn-icon left-margin"
					onClick={(event) => {
                       event.preventDefault();
                       this.onEnterValue(options, this.state.property,this.state.value);
                    }}
				  >
					  <i className="fa fa-plus" aria-hidden="true"></i>
				  </button>
			  </div>
			  <Notify
				state="error"
				message={this.state.message}
				shown={this.state.error}
			  />
		  </fieldset>;

		let addButton =
		  <button className="btn btn-primary bottom-margin" onClick={this.onOpen}>
			  <i className="fa fa-plus" aria-hidden="true"></i>
			  <span className="left-margin">Add</span>
		  </button>;

		let editButton =
		  <button className="btn btn-icon btn-secondary pull-xs-right" onClick={this.onOpen}>
			  <i className="fa fa-pencil-square-o" aria-hidden="true"></i>
			  <span className="left-margin">Edit</span>
		  </button>;


		return (
		  <div>
			  {this.state.method == 'ADD' && addButton}
			  {this.state.method == 'EDIT' && editButton}

			  <Modal show={this.state.visible} onHide={this.onClose}>
				  <Modal.Header closeButton>
					  <Modal.Title>{this.state.method == 'ADD' ? 'Add' : 'Edit'} Gate</Modal.Title>
				  </Modal.Header>
				  <Modal.Body>
					  <Notify
						state="alert alert-success"
						message="Gate successfully created!"
						shown={this.state.submitted}/>

					  <form onSubmit={handleSubmit(this.onSubmit)}>
						  <input type="hidden" {...id} />
						  <div className={validateGate(name)}>
							  <label>Name</label>
							  <input
								type="text"
								placeholder="Enter Gate Name"
								className="form-control"
								onEnter={event => event.preventDefault()}
								{...name} />
							  <Notify state="error" message={name.error} shown={name.touched && name.error}/>
						  </div>
						  {addForm}
						  {!options.length &&
						  <div className={`gate-option`}>No Options Added.</div>}
						  {options.map((option, index) => {
							  return (
								<GateOption
								  key={index}
								  index={index}
								  option={option}
								  options={options}/>
							  );
						  })}

						  <div className="form-group">
							  <button
								type="submit"
								className="btn btn-primary">
								  Save
							  </button>
							  <Button
								className="btn btn-secondary left-margin"
								onClick={this.onClose}>
								  Cancel
							  </Button>
						  </div>
					  </form>
				  </Modal.Body>
			  </Modal>
		  </div>
		);
	}
}

export default reduxForm({
	  fields,
	  validate
  },
  undefined,
  {
	  createGate,
	  updateGate,
	  removeArrayValue
  })(GateForm);