import {RETRIEVE_CAMPAIGNS} from '../actions/action-campaigns';

export default function (state = [], action) {
  switch (action.type) {
    case RETRIEVE_CAMPAIGNS:
      return action.payload.data;
  }

  return state;
}