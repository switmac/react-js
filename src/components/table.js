import React from 'react';
require('../styles/style.css');

export default(props) => {
  function headerList() {
    return props.headers.map(function (header) {
      return (
        <td key={header}>{header}</td>
      );
    });
  }

  return (
    <div className="table-section">
      <table className="table table-bordered table-striped">
        <thead className="thead-default">
        <tr id="theader">
          {headerList()}
        </tr>
        </thead>
        <tbody>
        {props.body}
        </tbody>
      </table>
    </div>
  );
}