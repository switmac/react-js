import {RETRIEVE_CUSTOMERS} from '../actions/index';

export default function (state = [], action) {
  switch (action.type) {
    case RETRIEVE_CUSTOMERS:
      return action.payload.data;
  }

  return state;
}