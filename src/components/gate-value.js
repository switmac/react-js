import React from 'react';

export default (props) => {
  const {index, value, option} = props;

  return (
    <button
      type="button"
      className="btn btn-secondary left-margin gate-value"
    >
      <span className="gate-option-value">{value}</span>
      <a
        className="left-margin"
        onClick={() => {option.values.removeField(index)}}>
        <i className="fa fa-times" aria-hidden="true"></i>
      </a>
    </button>
  );
}