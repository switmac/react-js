import React, {Component} from 'react';
import Sidebar from './../containers/sidebar';
import Header from './header';
import axios from 'axios';

export default class App extends Component {
  constructor(props) {
    super(props);
    axios.defaults.baseURL = 'http://localhost:1323';
  }

  render() {
    return (
      <div>
        <Header />
        <div className="viewport">
          <Sidebar />
          <div className="content col-sm-9 col-sm-offset-3 col-md-9 col-md-offset-2 main">
            {this.props.children}
          </div>
        </div>
      </div>
    );
  }
}