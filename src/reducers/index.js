import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';

import Campaigns from './reducer-campaigns';
import Paths from './reducer-paths';
import Gates from './reducer-gates';
import Customers from './reducer-customers';

const rootReducer = combineReducers({
  campaigns: Campaigns,
  paths: Paths,
  gates: Gates,
  customers: Customers,
  form: formReducer
});

export default rootReducer;
