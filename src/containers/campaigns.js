import React, {Component} from 'react';
import {connect} from 'react-redux';

import Table from '../components/table';
import SearchBar from '../components/search-bar';
import DomainEntry from '../containers/campaigns-domain-entry';
import {retrieveCampaigns} from '../actions/action-campaigns';
require('../styles/style.css');

class Campaigns extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.retrieveCampaigns();
  }

  renderBody(action) {
    return this.props.campaigns.map((domain, index) => {
      if (domain.action == action)
        return <DomainEntry key={index} domain={domain}/>
    });
  }

  render() {
    let heading = <div className="view-title">
      <h4>Campaigns</h4>
    </div>;

    if (!this.props.campaigns) {
      return (
        <div id="campaigns">
          {heading}
          <Table
            headers={['Domain', 'Visitors', 'Crawlers', 'Ratio', 'Actions']}
            body={<td key="1" className="colSpan" colSpan="5">No Campaigns Found</td>}
          />
        </div>
      );
    }

    return (
      <div id="campaigns">

        {heading}
        <ul className="nav nav-tabs" role="tablist">
          <li className="nav-item">
            <a className="nav-link active" data-toggle="tab" href="#live" role="tab">Live</a>
          </li>
          <li className="nav-item">
            <a className="nav-link" data-toggle="tab" href="#paused" role="tab">Paused</a>
          </li>
          <li className="nav-item">
            <a className="nav-link" data-toggle="tab" href="#deleted" role="tab">Deleted</a>
          </li>
          <li>
            <SearchBar/>
          </li>
        </ul>
        <div className="tab-content">
          <div className="tab-pane active" id="live" role="tabpanel">
            <Table
              headers={['Domain', 'Visitors', 'Crawlers', 'Ratio', 'Actions']}
              body={this.renderBody("Live")}
            />
          </div>
          <div className="tab-pane" id="paused" role="tabpanel">
            <Table
              headers={['Domain', 'Visitors', 'Crawlers', 'Ratio', 'Actions']}
              body={this.renderBody("Paused")}
            />
          </div>
          <div className="tab-pane" id="deleted" role="tabpanel">
            <Table
              headers={['Domain', 'Visitors', 'Crawlers', 'Ratio', 'Actions']}
              body={this.renderBody("Deleted")}
            />
          </div>
        </div>
      </div>
    );
  }
}


function mapStateToProps({campaigns}) {
  return {campaigns};
}

export default connect(mapStateToProps, {retrieveCampaigns})(Campaigns);