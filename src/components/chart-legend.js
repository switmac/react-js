import React from 'react';
require('../styles/chart.css');

export function ChartLegend(props) {
  return (
    <div className="chart-legend">
      <label>Legend :</label>
      {props.children}
    </div>
  );
}

export function ChartLegendEntry(props) {
  return (
    <div className='chart-legend-entry left-margin'>
      <span className={`chart-legend-color ${props.className}`}/>
      {props.title}
    </div>
  );
}
