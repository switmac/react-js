import React, {Component} from 'react';
import {Button, Modal} from "react-bootstrap";


class Confirmation extends Component {

	constructor(props) {
		super(props);
		this.state = {visible: false};
	}

	render() {
		return (
		  <div>
			  <button
				className={this.props.buttonStyle}
				onClick={() => this.setState({visible:true})}>
				  {this.props.icon}
				  {this.props.label}
			  </button>

			  <Modal show={this.state.visible} onHide={() => this.setState({visible:false})}>
				  <Modal.Header>
					  <Modal.Title>Confirm</Modal.Title>
				  </Modal.Header>
				  <Modal.Body>
					  {this.props.message}
				  </Modal.Body>
				  <Modal.Footer>
					  <button
						className="btn btn-primary"
						onClick={() => {
					    this.props.callback();
					    this.setState({visible:false});
					  }}>OK
					  </button>
					  <Button
						className="btn btn-secondary left-margin"
						onClick={() => this.setState({visible:false})}>
						  Cancel
					  </Button>
				  </Modal.Footer>
			  </Modal>
		  </div>
		);
	}
}

export default Confirmation;