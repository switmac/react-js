import React, {Component} from 'react';

class SearchBar extends Component {
  render() {
    return (
      <div className="search-bar pull-xs-right form-inline col-md-8 col-xs-8 input-group">
        <input type="text" className="form-control" placeholder="Search" aria-describedby="search-addon"/>
        <span className="btn btn-primary input-group-addon" id="search-addon">Search</span>
      </div>
    );
  }
}

export default SearchBar;