import React, {Component} from 'react';
import {connect} from 'react-redux';

import {retrieveCustomers} from '../actions/index';
import Table from '../components/table';
import SearchBar from '../components/search-bar';

class Customers extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.retrieveCustomers();
  }

  renderBody() {
    return this.props.customers.map((customer, index) => {
      return (
        <tr key={index}>
          <td key={`${customer.lastName}${customer.firstName}-firstName`}>{customer.firstName}</td>
          <td key={`${customer.lastName}${customer.firstName}-lastName`}>{customer.lastName}</td>
          <td key={`${customer.lastName}${customer.firstName}-email`}>{customer.email}</td>
          <td key={`${customer.lastName}${customer.firstName}-phone`}>{customer.phone}</td>
          <td key={`${customer.lastName}${customer.firstName}-address`}>{customer.address}</td>
          <td key={`${customer.lastName}${customer.firstName}-city`}>{customer.city}</td>
          <td key={`${customer.lastName}${customer.firstName}-state`}>{customer.state}</td>
          <td key={`${customer.lastName}${customer.firstName}-zip`}>{customer.zip}</td>
        </tr>
      );
    });
  }

  render() {
    let heading = <div className="view-title">
      <h4>Customers</h4>
      <SearchBar />
    </div>;

    if (!this.props.customers) {
      return (
        <div id="customers">
          {heading}
          <Table
            headers={['First Name', 'Last Name', 'Email Address', 'Phone Number', 'Address', 'City', 'State', 'Zip']}
            body={<tr><td colSpan="8" className="colSpan">No Customers Found</td></tr>}
          />
        </div>
      );
    }

    return (
      <div id="customers">
        {heading}
        <Table
          headers={['First Name', 'Last Name', 'Email Address', 'Phone Number', 'Address', 'City', 'State', 'Zip']}
          body={this.renderBody()}
        />
      </div>
    );
  }
}

function mapStateToProps({customers}) {
  return {customers};
}

export default connect(mapStateToProps, {retrieveCustomers})(Customers);
