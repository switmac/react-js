import React, {Component}from 'react';
import {Link} from 'react-router';
require('../styles/style.css');

class Sidebar extends Component {

  constructor(props) {
    super(props);
    this.state = {active: "dashboard"};
  }

  isSelected(value) {
    return `${(value === this.state.active) ? 'active' : ''}`;
  }

  renderList() {
    var links = [
      {id: 'dashboard', icon: 'fa fa-fw fa-area-chart', text: 'Dashboard', path: '/'},
      {id: 'campaigns', icon: 'fa fa-fw fa-server', text: 'Campaigns', path: '/campaigns'},
      {id: 'gates', icon: 'fa fa-fw fa-filter', text: 'Gates', path: '/gates'},
      {id: 'customers', icon: 'fa fa-fw fa-users', text: 'Customers', path: '/customers'}
    ];

    return links.map((nav) => {
      return (
        <Link
          to={nav.path}
          className={this.isSelected(nav.id)+ " list-group-item"}
          key={nav.id}
          onClick={event => this.setState({active:nav.id})}
        >
          <strong>{nav.text}</strong>
          <i className={`${nav.icon} pull-xs-right`} aria-hidden="true"></i>
        </Link>
      );
    });
  }

  render() {
    return (
      <div id="sidebar" className="col-md-3 col-sm-2">
        <ul className="nav nav-sidebar nav-pills nav-stacked list-group">
          {this.renderList()}
        </ul>
      </div>
    );
  }
}

export default Sidebar;