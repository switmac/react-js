import React, {Component} from 'react';
import DatePicker from 'react-datepicker';
require('react-datepicker/dist/react-datepicker.css');

import Table from '../components/table';
import Chart from '../components/chart';
import Rules from '../containers/campaigns-path-rules';
import {ChartLegend, ChartLegendEntry} from '../components/chart-legend';

class Path extends Component {

  constructor(props) {
    super(props);

    this.state = {
      host: this.props.params.host,
      path: this.props.params.path
    };
  }

  getData() {
    var impression = [];
    for (var i = 0; i < 10; i++) {
      impression[i] = (Math.random() * 100 % (i + 1)) * 100;
    }

    var clicks = [];
    for (var i = 0; i < 10; i++) {
      clicks[i] = (Math.random() * 100 % (i + 1)) * 100;
    }

    var crawlers = [];
    for (var i = 0; i < 10; i++) {
      crawlers[i] = (Math.random() * 100 % (i + 1)) * 100;
    }


    return {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [
        {
          label: "Impression",
          fillColor: "rgba(110,110,110,0.1)",
          strokeColor: "rgba(110,110,110 ,0.5)",
          pointColor: "rgba(110,110,110 ,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(110,110,110,1)",
          data: impression
        },
        {
          label: "Clicks",
          fillColor: "rgba(255,69,0,0.1)",
          strokeColor: "rgba(255,69,0,0.5)",
          pointColor: "rgba(255,69,0,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(255,69,0,1)",
          data: clicks
        },
        {
          label: "Crawlers",
          fillColor: "rgba(20,69,0,0.1)",
          strokeColor: "rgba(20,69,0,0.5)",
          pointColor: "rgba(20,69,0,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(20,69,0,1)",
          data: crawlers
        }
      ]
    };
  }

  getOptions() {
    return {
      stacked: true,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    };
  }

  render() {
    return (
      <div>
        <div className="view-title">
          <div className="pull-xs-left">
            <h6 className="text-muted">PATH</h6>
            <h4>{this.state.host}/{this.state.path}</h4>
          </div>
          <div className="pull-xs-right form-inline form-group">
            <DatePicker
              className="date-picker form-control left-margin"
              selected={this.state.startDate}
              isClearable={true}
              placeholderText="Start Date"
              title="Start Date"
              onChange={(date) => {this.setState({startDate: date})}}
            />
            <DatePicker
              className="date-picker form-control left-margin"
              selected={this.state.endDate}
              isClearable={true}
              placeholderText="End Date"
              title="Start Date"
              tabIndex={4}
              onChange={(date) => {
              this.setState({endDate: date});
            }}
            />
            <Rules host={this.state.host} path={this.state.path} />
          </div>
        </div>
        <div>
          <Chart
            callback={this.getData}
            width="800"
            height="300"
            options={this.getOptions()}
            interval={20000}
          />
          <ChartLegend>
            <ChartLegendEntry className="impression-legend" title="Impression"/>
            <ChartLegendEntry className="click-legend" title="Clicks"/>
            <ChartLegendEntry className="crawler-legend" title="Crawlers"/>
          </ChartLegend>
        </div>
        <div>
          <h5>Click Log</h5>
          <Table
            headers={['Time', 'IP', 'User Agent', 'Country', 'Org', 'ISP', 'Carrier', 'Browser', 'Platform', 'Host', 'Filtered']}
            body={<tr><td className="colSpan" colSpan="11">No Click Logs Found</td></tr>}
          />
        </div>
      </div>
    );
  }
}

export default Path;