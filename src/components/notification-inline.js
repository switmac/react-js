import React from 'react';
require('../styles/notification.css');

export default (props) => {
	var shown = props.shown;

	if (!shown) {
		return <span></span>;
	}

	return (
	  <div
		className={`text-help ${props.state}`}>
		  {props.message}
	  </div>
	);
}