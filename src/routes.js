import React from 'react';
import {Route, IndexRoute} from 'react-router';

import App from './components/app';
import Dashboard from './containers/dashboard';
import Campaigns from './containers/campaigns';
import Domain from './containers/campaigns-domain';
import Path from './containers/campaigns-path';
import Gates from './containers/gates';
import Customers from './containers/customers';
import NotFound from './components/not-found';

export default(
  <Route path="/" component={App}>
    <IndexRoute component={Dashboard}/>
    <Route path="dashboard" component={Dashboard}/>
    <Route path="campaigns" component={Campaigns}/>
    <Route path="campaigns/:host" component={Domain}/>
    <Route path="campaigns/:host/:path" component={Path}/>
    <Route path="gates" component={Gates}/>
    <Route path="customers" component={Customers}/>
    <Route path="*" component={NotFound} status={404}/>
  </Route>
);