import React, {Component, PropTypes} from 'react';
import {connect} from 'react-redux';
import NotificationSystem from 'react-notification-system';

import {hide} from '../actions/action-notification';

class Notification extends Component {
  static propTypes = {
    notifications: PropTypes.array
  }

  constructor(props) {
    super(props);
  }

  system() {
    return this.refs.notify;
  }

  componentWillReceiveProps(nextProps) {
    const {notifications} = nextProps;

    notifications.map((notification) => {
      this.system().addNotification({
        ...notification,
        onRemove: () => {
          this.context.store.dispatch(hide(notification.uid));
        }
      });
    });
  }

  shouldComponentUpdate(nextProps) {
    return this.props !== nextProps;
  }

  componentDidMount() {
    this.state._notificationSystem = this.refs.notificationSystem;
  }

  addNotification(message, level) {
    this.state._notificationSystem.addNotification({
      message: message,
      level: level
    });
  }

  render() {
    return (
      <NotificationSystem ref="notificationSystem"/>
    );
  }
}

Notification.contextTypes = {
  store: PropTypes.object
};

export default connect(
  Notification,
  state => ({
    state : state.notifications
  })
);