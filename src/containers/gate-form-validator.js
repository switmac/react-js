export function validate(values) {
	const errors = {};

	if (!values.name) {
		errors.name = 'Required';
	}

	return errors;
}

export function validateValueEntry(options, property, value) {
	var validation = {
		propertyExists: false,
		valueExists: false,
		empty: false,
		option: null
	};

	if (!property || !value) {
		validation.empty = true;
	}

	options.map((option) => {
		if (option.property.value == property) {
			validation.propertyExists = true;
			validation.option = option;

			option.values.map((optionValue) => {
				if (value == optionValue.value) {
					validation.valueExists = true;

				}
			});
		}
	});

	return validation;
}

export function validateGate(control) {
	return `form-group ${control.touched && control.invalid ? 'has-danger' : ''}`;
}

export function validateOptions(values) {
	var validation = {
		propertyEmpty: false,
		valueEmpty: false,
		optionEmpty: false
	};

	if (values.options.length == 0) {
		validation.optionEmpty = true;
	}

	values.options.map((option, index) => {
		if (!option.property) {
			validation.propertyEmpty = true;
		}

		if (option.values.length == 0) {
			validation.valueEmpty = true;
		}
	});

	return validation;
}