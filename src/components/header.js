import React, {Component} from 'react';
import {Link} from 'react-router';
import {ButtonGroup} from 'react-bootstrap';
require('../styles/style.css');

export default (props) => {
  return (
    <nav id="heading" className="col-md-10">
      <div className="navbar-header">
        <Link className="navbar-brand" to="/">
          <i className="fa fa-th-large fa-3" aria-hidden="true"></i>
          <h3>Traffic Gate</h3>
        </Link>

        <div className="pull-xs-right">
          <ButtonGroup>
            <button className="btn btn-secondary">Plans</button>
            <button className="btn btn-primary">Logout</button>
          </ButtonGroup>
        </div>
      </div>
    </nav>
  );
}