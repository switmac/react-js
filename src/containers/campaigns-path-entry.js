import React, {Component} from 'react';
import {Link} from 'react-router';

export default (props) => {
  const {key, path, host} = props;
  return (
    <tr key={key}>
      <td key={`${path.path}-path`}><Link to={`/campaigns/${host}${path.path}`}>{path.path}</Link></td>
      <td key={`${path.path}-time`}>{path.time}</td>
    </tr>
  );
}
