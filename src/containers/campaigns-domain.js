import React, {Component} from 'react';
import {connect} from 'react-redux';

import {retrievePaths} from '../actions/action-paths';
import Table from '../components/table';
import PathEntry from './campaigns-path-entry';

require('../styles/style.css');

class Domain extends Component {

  constructor(props) {
    super(props);

    this.state = {host: ''};
    this.renderBody = this.renderBody.bind(this);
  }

  componentWillMount() {
    this.props.retrievePaths(this.props.params.host);
  }

  componentDidMount() {
    this.setState({host: this.props.params.host});
  }

  renderBody() {
    return this.props.paths.map((path, index) => {
      return (<PathEntry key={index} host={this.state.host} path={path}/>);
    });
  }
  
  render() {
    let heading = <div className="view-title">
      <h6 className="text-muted">DOMAIN</h6>
      <h4>{this.state.host}</h4>
    </div>;

    if (!this.props.paths) {
      return (
        <div id="paths">
          {heading}
          <Table
            headers={['Path', 'Visitors', 'Crawlers', 'Ratio', 'Actions']}
            body={<tr><td colSpan="5" className="colSpan">No Path Found</td></tr>}
          />
        </div>
      );
    }

    return (
      <div id="domains">
        <div id="paths">
          {heading}
          <Table
            headers={['Path', 'Visitors', 'Crawlers', 'Ratio', 'Actions']}
            body={this.renderBody()}
          />
        </div>
      </div>
    );
  }
}

function mapStateToProps({paths}) {
  return {paths};
}

export default connect(mapStateToProps, {retrievePaths})(Domain);