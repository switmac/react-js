import axios from 'axios';

export const RETRIEVE_CAMPAIGNS = 'RETRIEVE_CAMPAIGNS';
export const SAVE_CAMPAIGN = 'SAVE_CAMPAIGN';
export const SAVE_DOMAIN_ACTION = 'SAVE_DOMAIN_ACTION';

export function retrieveCampaigns() {
  const request = axios.get(`/sites`);

  return {
    type: RETRIEVE_CAMPAIGNS,
    payload: request
  };
}

export function saveDomainAction(props) {
  const request = axios.post('/sites/save', props);

  return {
    type: SAVE_DOMAIN_ACTION,
    payload: request
  };
}

export function saveCampaign(props) {
  const request = axios.post('/campaign/save', props);

  return {
    type: SAVE_CAMPAIGN,
    payload: request
  }
}