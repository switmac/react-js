import {RETRIEVE_PATHS, RETRIEVE_PATH} from '../actions/action-paths';

export default function (state = [], action) {
  switch (action.type) {
    case RETRIEVE_PATHS:
      return action.payload.data;
    case RETRIEVE_PATH:
      return action.payload.data;
  }

  return state;
}