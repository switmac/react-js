import React, {Component} from 'react';
import Chart from '../components/chart';

class Dashboard extends Component {
  constructor(props) {
    super(props);
  }

  getData() {
    var values = [];
    for (var i = 0; i < 10; i++) {
      values[i] = (Math.random() * 100 % (i + 1)) * 100;
    }

    return {
      labels: ["January", "February", "March", "April", "May", "June", "July"],
      datasets: [
        {
          label: "Data Set 2",
          fillColor: "rgba(110,110,110,0.1)",
          strokeColor: "rgba(10,10,10 ,0.5)",
          data: values
        },
        {
          label: "Data Set 1",
          fillColor: "rgba(255,69,0,0.1)",
          strokeColor: "rgba(255,69,0,0.5)",
          data: [65, 59, 80, 81, 56, 55, 40]
        }
      ]
    };
  }

  getOptions() {
    return {
      stacked: true,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    };
  }

  render() {
    return (
      <div id="dashboard">
        <div className="view-title">
          <h4>Overall Health</h4>
          <Chart
            callback={this.getData}
            width="900"
            height="500"
            options={this.getOptions()}
            interval={20000}
          />
        </div>
      </div>
    );
  }
}

export default Dashboard;
