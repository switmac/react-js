import React, {Component} from 'react';
import {Line} from 'react-chartjs';

class Chart extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      options: this.props.options
    };

    this.getData = this.getData.bind(this);

    setInterval(() => {
      this.getData();
    }, this.props.interval);
  }

  componentDidMount() {
    this.getData();
  }

  getData() {
    this.setState({data: this.props.callback()});
  }

  render() {

    if (this.state.data.length < 1) {
      return <div>Loading...</div>;
    }

    return (
      <div className="chart">
        <Line
          data={this.state.data}
          options={this.state.options}
          width={this.props.width}
          height={this.props.height}
        />
      </div>
    );
  }
}

export default Chart;