import React, {Component} from 'react';

import GateValue from '../components/gate-value';
import Notify from '../components/notification-inline';

export default (props) => {
	const {index, option, options} = props;

	if (!option) {
		return <div></div>;
	}

	return (
	  <fieldset className="form-control gate-option">
		  <button
			role="button"
			aria-label="Remove"
			className="btn btn-icon btn-danger btn-remove"
			onClick={(event) => {
          event.preventDefault();
          options.removeField(index);
        }}
		  >
			  <i className="fa fa-trash" aria-hidden="true"></i>
		  </button>
		  <legend>{option.property.value}</legend>

		  <div className="form-inline gate-value-list">
			  <Notify state="error" message="No Value Added." shown={(option.values == 0)} />
			  {option.values.map((value, index) => {
				  return (
					<GateValue
					  key={index}
					  index={index}
					  value={value.value}
					  option={option}
					/>
				  );
			  })}
		  </div>
	  </fieldset>
	);
}